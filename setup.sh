#!/bin/bash
HOSTS_CONF_DIRECTORY=~/.hosts.d
MANUAL_HOSTS=$HOSTS_CONF_DIRECTORY/manual-additions.hosts;
WHITE_LIST=$HOSTS_CONF_DIRECTORY/white-list.hosts;

if [ ! -d $HOSTS_CONF_DIRECTORY ]; then
  echo "Creating configuration directory ...";
  mkdir -p $HOSTS_CONF_DIRECTORY;
  touch $MANUAL_HOSTS;
  touch $WHITE_LIST;
  echo "To add your own websites to block, append them to $MANUAL_HOSTS then re-run this script"
  echo "To unblock specific websites, append them to $WHITE_LIST then re-run this script"
fi

echo "Pushing scripts to the hosts.d directory"
cp ./populate_hosts.sh $HOSTS_CONF_DIRECTORY/

echo 'alias refresh_hosts="sudo sh ~/.hosts.d/populate_hosts.sh"' >> ~/.zshrc
source ~/.zshrc

#TODO: figure out why this is not functioning as expected
# The entry is correctly made in crontab, but when run, it lacks rights

#echo "Setting up cron rerun the scripts on system reboot"
#crontab -l > temporary_crontab
#echo "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin" >> temporary_crontab
#echo "@reboot /bin/bash ${HOSTS_CONF_DIRECTORY}/populate_hosts.sh > /etc/hosts.d/out.log 2> /etc/hosts.d/err.log" >> temporary_crontab
#crontab temporary_crontab
#rm temporary_crontab

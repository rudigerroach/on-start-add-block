#!/bin/bash
SOURCE_HOSTS=("https://block.energized.pro/bluGo/formats/hosts" "https://block.energized.pro/porn/formats/hosts")
HOSTS_CONF_DIRECTORY=~/.hosts.d
AGGREGATED_HOSTS_FILE=$HOSTS_CONF_DIRECTORY/aggregated.hosts
MANUAL_HOSTS=$HOSTS_CONF_DIRECTORY/manual-additions.hosts;
WHITE_LIST=$HOSTS_CONF_DIRECTORY/white-list.hosts;
SYSTEM_HOSTS_FILE=/etc/hosts

echo "To add your own host entries, append them to $MANUAL_HOSTS then re-run this script"

if [ -e $AGGREGATED_HOSTS_FILE ]; then
  echo "Removing prevously created hosts list ...";
  rm $AGGREGATED_HOSTS_FILE;
  touch $AGGREGATED_HOSTS_FILE;
fi

touch hosts.tmp
for host in ${SOURCE_HOSTS[@]}; do
  echo "Getting fresh hosts from $host ..."
  curl $host -o hosts.tmp
  cat hosts.tmp >> $AGGREGATED_HOSTS_FILE
done
rm hosts.tmp

echo "Appending your personal entries to the hosts file ..."
cat $MANUAL_HOSTS $AGGREGATED_HOSTS_FILE > $SYSTEM_HOSTS_FILE

echo "whitelisting hosts in $WHITE_LIST ..."
while IFS= read -r line; do
  sed -i '' '/'$line'/d' $SYSTEM_HOSTS_FILE
done < $WHITE_LIST

echo "Your hosts list is now up to date"

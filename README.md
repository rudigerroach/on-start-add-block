Set up a system-wide add blocker

To set it up run `./setup.sh` from your terminal.
To activate the adblocking voodoo run `refresh_hosts` .
Periodically run `refresh_hosts` to update the block lists.
